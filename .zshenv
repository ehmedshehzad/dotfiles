export ZDOTDIR=$HOME/.config/zsh
export  EDITOR=/usr/bin/nvim
export GPG_TTY=$TTY
export    PATH=$PATH:$HOME/.local/bin
export LESSHISTFILE=/dev/null
