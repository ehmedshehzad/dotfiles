# Lines configured by zsh-newuser-install
HISTFILE=$ZDOTDIR/history
HISTSIZE=10000
SAVEHIST=100000
setopt autocd extendedglob nomatch notify inc_append_history
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename $ZDOTDIR/.zshrc

autoload -Uz compinit
compinit
# End of lines added by compinstall

PROMPT='%B%F{cyan}[%~]%f%b '

source $ZDOTDIR/aliases
